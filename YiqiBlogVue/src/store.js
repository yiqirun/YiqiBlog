import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

/**Vuex配置文件 */
export default new Vuex.Store({
    state: {
        adminmenuactiveindex: ''
    },

    mutations: {

    },

    actions: {

    }
});
