package run.yiqi.blog;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import run.yiqi.blog.dao.EndUserDao;
import run.yiqi.util.StringUtil;

@Slf4j
@Component
@Order(value = 1)
public class InitDatabase implements CommandLineRunner {

    @Autowired
    @Lazy
    private EndUserDao endUserDao;

    @Autowired
    @Lazy
    private SqlDao sqlDao;

    @Override
    public void run(String... args) throws Exception {
        log.debug("");
        long c = endUserDao.count();
        if (c == 0) {
            ClassPathResource cpr = new ClassPathResource("init.sql");
            @Cleanup
            InputStreamReader isr = new InputStreamReader(cpr.getInputStream(), "UTF-8");
            @Cleanup
            BufferedReader br = new BufferedReader(isr);
            List<String> sqls = new ArrayList<String>();

            String sql = null;
            while ((sql = br.readLine()) != null) {
                if (sql.startsWith("--") || StringUtil.isEmpty(sql.trim())) {
                    continue;
                }
                sqls.add(sql);
            }
            this.sqlDao.executeSql(sqls);
        }
    }

}

@Component
class SqlDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void executeSql(List<String> sqls) throws Exception {
        sqls.forEach(sql -> {
            Query query = entityManager.createNativeQuery(sql);
            query.executeUpdate();
        });
    }

}
