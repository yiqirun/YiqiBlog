package run.yiqi.blog.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class MvcConfig implements WebMvcConfigurer {
    /**
     * This is a shortcut for defining a ParameterizableViewController that
     * immediately forwards to a view when invoked. You can use it in static cases
     * when there is no Java controller logic to execute before the view generates
     * the response.
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // swagger
        registry.addResourceHandler("/swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");

        // vue
        registry.addResourceHandler("/vue/**").addResourceLocations("classpath:/static/vue/");

        WebMvcConfigurer.super.addResourceHandlers(registry);
    }

}
