package run.yiqi.blog.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.CriteriaBuilder.In;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import run.yiqi.blog.dao.BlogDao;
import run.yiqi.blog.dao.TagOptionDao;
import run.yiqi.blog.dao.TagTypeDao;
import run.yiqi.blog.dao.es.BlogEsDao;
import run.yiqi.blog.model.Blog;
import run.yiqi.blog.model.BlogEs;
import run.yiqi.blog.vo.BlogVo;
import run.yiqi.blog.vo.TagOptionVo;
import run.yiqi.util.DateTimeUtil;
import run.yiqi.util.QueryCondition;
import run.yiqi.util.StringUtil;

@Slf4j
@Controller
public class BlogAdminController {
	
	@Lazy @Autowired private BlogDao blogDao;
	@Lazy @Autowired private BlogEsDao blogEsDao;
	@Lazy @Autowired private TagTypeDao tagTypeDao;
	@Lazy @Autowired private TagOptionDao tagOptionDao;
	@Value("${x.es}") private boolean es;

    @ResponseBody
    @RequestMapping(value = { "/api/admin/blogadmin/query/listBlogs" }, method = RequestMethod.POST)
    public QueryCondition<List<BlogVo>> listBlogs(@RequestBody QueryCondition<List<BlogVo>> queryCondition) {
        log.info("listBlogs ==> " + queryCondition.toString());

        PageRequest pageRequest = null;
        if(!StringUtil.isEmpty(queryCondition.getSort().getField())) {
            pageRequest = PageRequest.of(queryCondition.getPage().getCurrentPage() - 1, 
                    queryCondition.getPage().getPageSize(),
                    Sort.by(queryCondition.getSort().getDirection(), queryCondition.getSort().getField()));
        } else {
            pageRequest = PageRequest.of(queryCondition.getPage().getCurrentPage() - 1, 
                    queryCondition.getPage().getPageSize(),
                    //Sort.by(Sort.Direction.DESC, "createDate"));
                    Sort.by(Sort.Direction.ASC, "title"));
        }
        
        
        Page<Blog> blogs = this.blogDao.findAll(new Specification<Blog>() {
            private static final long serialVersionUID = 1L;
            
            @Override
            public Predicate toPredicate(Root<Blog> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<Predicate>();
                
                queryCondition.getFilters().stream().forEach(filter->{
                    switch(filter.getField()) {
                        case "id":
                            In<String> idin = criteriaBuilder.in(root.get("id"));
                            Arrays.asList(filter.getValues()).forEach(v->{
                                idin.value(v.toString());
                            });
                            predicates.add(idin);
                            break;
                            
                        case "keyword": 
                            //title/comment/content
                            if(filter.getValues()[0].toString().trim().length()>0) {
                                Predicate keyword = criteriaBuilder.or(criteriaBuilder.like(root.get("title").as(String.class), "%"+filter.getValues()[0].toString().trim() + "%"), criteriaBuilder.like(root.get("content").as(String.class), "%"+filter.getValues()[0].toString().trim() + "%"));
                                predicates.add(keyword);
                            }
                            break;

                        case "createBy": 
                            if(filter.getValues().length>0) {
                                In<String> createBy = criteriaBuilder.in(root.get("createBy").as(String.class));
                                Arrays.asList(filter.getValues()).forEach(v->{
                                    createBy.value(v.toString());
                                });
                                predicates.add(createBy);
                            }
                            break;
                            
                        case "createDate": 
                            if(filter.getValues().length == 2) {
                                predicates.add(criteriaBuilder.between(root.get("createDate").as(LocalDateTime.class), LocalDateTime.parse(filter.getValues()[0]+" 00:00:00", DateTimeFormatter.ofPattern(DateTimeUtil.DATETIMEFORMAT002)), LocalDateTime.parse(filter.getValues()[1]+ " 00:00:00", DateTimeFormatter.ofPattern(DateTimeUtil.DATETIMEFORMAT002))));
                            }
                            break;
                            
                        case "modifyBy": 
                            if(filter.getValues().length>0) {
                                In<String> modifyBy = criteriaBuilder.in(root.get("modifyBy").as(String.class));
                                Arrays.asList(filter.getValues()).forEach(v->{
                                    modifyBy.value(v.toString());
                                });
                                predicates.add(modifyBy);
                            }
                            break;
                            
                        case "updateDate":
                            if(filter.getValues().length == 2) {
                                predicates.add(criteriaBuilder.between(root.get("updateDate").as(LocalDateTime.class), LocalDateTime.parse(filter.getValues()[0]+" 00:00:00", DateTimeFormatter.ofPattern(DateTimeUtil.DATETIMEFORMAT002)), LocalDateTime.parse(filter.getValues()[1]+ " 00:00:00", DateTimeFormatter.ofPattern(DateTimeUtil.DATETIMEFORMAT002))));
                            }
                            break;
                            
                        case "tags":
                            if(filter.getValues().length>0) {
                                In<String> tags = criteriaBuilder.in(root.join("tags", JoinType.LEFT).get("optionid").as(String.class));
                                Arrays.asList(filter.getValues()).forEach(v->{
                                    tags.value(v.toString());
                                });
                                predicates.add(tags);
                            }
                            break;

                        default: 
                            break;
                            
                    }
                });
                
                if(root.getJoins().stream().map(j->j.getAttribute().getName()).filter(n->"tags".equals(n)).count()==0) {
                    root.join("tags", JoinType.LEFT);
                }

                query.distinct(true);

                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
            
        },pageRequest);

        queryCondition.getPage().setTotal(blogs.getTotalElements());
        queryCondition.getPage().setPageSize(queryCondition.getPage().getPageSize());
        queryCondition.getPage().setCurrentPage(queryCondition.getPage().getCurrentPage());
        queryCondition.setData(new ArrayList<BlogVo>());

        blogs.forEach(blog->{
            BlogVo blogVo = this.convertBlog2BlogVo(blog);
            queryCondition.getData().add(blogVo);
        });

        if (queryCondition.getData().size() == 0 && queryCondition.getPage().getCurrentPage() > 1) {
            queryCondition.getPage().setCurrentPage(queryCondition.getPage().getCurrentPage() - 1);
            queryCondition.getData().clear();
            queryCondition.getData().addAll(this.listBlogs(queryCondition).getData());
        }
        return queryCondition;
    }
	
    @Transactional
	@SneakyThrows
	@ResponseBody
	@RequestMapping(value="/api/admin/blogadmin/admin/saveBlog", method=RequestMethod.POST)  
	public BlogVo saveBlog(@RequestBody BlogVo blogVo) {
	    log.info("saveBlog "+blogVo.getStar());
		final Blog blog;
		
		if(StringUtil.isEmpty(blogVo.getId())) {
			blog = new Blog();
			blog.setContent(blogVo.getContent());
			blog.setTitle(blogVo.getTitle());
			blog.setStar(blogVo.getStar());
			blog.setTags(blogVo.getTags().stream().map(tv->this.tagOptionDao.findById(tv.getOptionid()).get()).collect(Collectors.toSet()));
			blog.getTags().forEach(t->t.getBlogs().add(blog));
			
		} else {
			blog = blogDao.findById(blogVo.getId()).get();
			if(!StringUtil.isEmpty(blogVo.getContent())) {
                blog.setContent(blogVo.getContent());
            }
			blog.setTitle(blogVo.getTitle());
			blog.setStar(blogVo.getStar());
			
			blog.setTags(blogVo.getTags().stream().map(tv->this.tagOptionDao.findById(tv.getOptionid()).get()).collect(Collectors.toSet()));
		}
		this.blogDao.saveAndFlush(blog);
		
		if(es) {
			BlogEs blogEs = new BlogEs();
			blogEs.setId(blog.getId());
			blogEs.setTitle(blog.getTitle());
			blogEs.setContent(blog.getContent());
			this.blogEsDao.save(blogEs);
		}
		return blogVo;
	}
	
	@SneakyThrows
	@ResponseBody
	@RequestMapping(value="/api/admin/blogadmin/admin/deleteBlog/{id}", method=RequestMethod.GET)  
	public void deleteBlog(@PathVariable String id) {
	    log.info("deleteBlog");
		this.blogDao.deleteById(id);
		if(es) this.blogEsDao.deleteById(id);
		return;
	}
	
	@ResponseBody
    @RequestMapping(value = { "/api/admin/blogadmin/query/getBlog/{id}" }, method = RequestMethod.GET)
    public BlogVo getBlog(@PathVariable String id) {
        log.info("getBlog");
        Blog blog = this.blogDao.findById(id).get();
        BlogVo blogVo = this.convertBlog2BlogVo(blog);
        blogVo.setContent(blog.getContent());

        return blogVo;
    }
	
	private BlogVo convertBlog2BlogVo(Blog blog) {
	    BlogVo blogVo = new BlogVo();
        blogVo.setId(blog.getId());
        blogVo.setTitle(blog.getTitle());
        //blogVo.setContent(blog.getContent());
        blogVo.setCreateBy(blog.getCreateBy());
        blogVo.setCreateDate(blog.getCreateDate().format(DateTimeFormatter.ofPattern(DateTimeUtil.DATETIMEFORMAT002)));
        blogVo.setModifyBy(blog.getModifyBy());
        blogVo.setUpdateDate(blog.getUpdateDate().format(DateTimeFormatter.ofPattern(DateTimeUtil.DATETIMEFORMAT002)));
        blogVo.setStar(blog.getStar());
        blogVo.setTags(blog.getTags().stream().map(t->{
            TagOptionVo tagOptionVo = new TagOptionVo();
            tagOptionVo.setOptionid(t.getOptionid());
            tagOptionVo.setOptionname(t.getOptionname());
            tagOptionVo.setTypeid(t.getTagtype().getTypeid());
            return tagOptionVo;
        }).collect(Collectors.toList()));
        return blogVo;
	}
}