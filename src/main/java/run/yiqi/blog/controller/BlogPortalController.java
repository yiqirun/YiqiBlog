package run.yiqi.blog.controller;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import lombok.extern.slf4j.Slf4j;
import run.yiqi.blog.dao.BlogDao;
import run.yiqi.blog.dao.EndUserDao;
import run.yiqi.blog.dao.RoleDao;
import run.yiqi.blog.dao.TagOptionDao;
import run.yiqi.blog.dao.TagTypeDao;
import run.yiqi.blog.dao.es.BlogEsDao;
import run.yiqi.blog.model.Blog;
import run.yiqi.blog.model.BlogEs;
import run.yiqi.blog.model.TagOption;
import run.yiqi.blog.vo.BlogVo;
import run.yiqi.blog.vo.TagOptionVo;
import run.yiqi.blog.vo.TagTypeVo;
import run.yiqi.util.DateTimeUtil;
import run.yiqi.util.QueryCondition;
import run.yiqi.util.StringUtil;

@Slf4j
@Controller
public class BlogPortalController { 

    @Lazy @Autowired private TagTypeDao tagTypeDao;
    @Lazy @Autowired private TagOptionDao tagOptionDao;
    @Lazy @Autowired private TagController tagController;
    @Lazy @Autowired private SystemParameterController systemParameterController;
    @Lazy @Autowired private RoleDao roleDao;
    @Lazy @Autowired private EndUserDao endUserDao;
    @Lazy @Autowired private BlogDao blogDao;
    @Lazy @Autowired private BlogEsDao blogEsDao;
    @Value("${x.es}") private boolean es;
    
    /**
     * 根据标签id来获取博客列表, 标签不传表示所有博客
     * 
     * @param queryCondition
     * @param tagid
     * @return
     */
    @RequestMapping(value = { "/" }, method=RequestMethod.GET)
    public String listBlogs() {
        log.info("listBlogs ==> home");
        return "redirect:/blogs.html";
    }
   
    /**
     * 根据标签id来获取博客列表, 标签不传表示所有博客
     * 
     * @param queryCondition
     * @param tagid
     * @return
     */
    @RequestMapping(value = { "/blogs.html" }, name = "/blogs.html", method=RequestMethod.GET)
    public ModelAndView listBlogs(@RequestParam(required=false, defaultValue="1") int currentPage, @RequestParam(required=false, defaultValue="") String tagoptionid) {
        log.info("listBlogs ==> " + currentPage + " tagoptionid="+tagoptionid);

        QueryCondition<List<BlogVo>> queryCondition = new QueryCondition<List<BlogVo>>();
        queryCondition.getPage().setCurrentPage(currentPage);
        
        return this.listBlogs(queryCondition, tagoptionid);
    }
    
    /**
     * 根据博客ID来获取博客详细内容
     * 
     * @param blogid
     * @return
     */
    @RequestMapping(value = { "/blog.html" }, method = RequestMethod.GET)
    public ModelAndView getBlog(@RequestParam(required=true) String blogid) {
        Blog blog = this.blogDao.findById(blogid).get();
        BlogVo blogVo = this.convertBlog2BlogVo(blog);
        blogVo.setContent(blog.getContent());
        
        ModelAndView mv = new ModelAndView();
        mv.setViewName("blog.html");
        mv.addObject("systemParameter", this.systemParameterController.listSysParams());
        mv.addObject("blog", blogVo);
        return mv;
    }
    
    /**
     * 全文检索
     * @param keyword
     * @return
     */
    @RequestMapping(value = { "/search.html" }, method = RequestMethod.GET)
    public ModelAndView search(@RequestParam(required=true) String keyword) {
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(new QueryStringQueryBuilder(keyword))
                .withSort(SortBuilders.scoreSort().order(SortOrder.DESC))
                .build();

        Page<BlogEs> blogEs = this.blogEsDao.search(searchQuery);

        ModelAndView mv = new ModelAndView();
        mv.setViewName("search.html");
        mv.addObject("systemParameter", this.systemParameterController.listSysParams());
        mv.addObject("blogVos", blogEs);
        mv.addObject("keyword", keyword);
        return mv;
    }
    
    
    
    //查询
    private ModelAndView listBlogs(QueryCondition<List<BlogVo>> queryCondition, String tagoptionid) {
        PageRequest pageRequest = PageRequest.of(queryCondition.getPage().getCurrentPage() - 1, queryCondition.getPage().getPageSize(), Sort.by(Direction.ASC, "title"));
        
        queryCondition = queryNConvert(pageRequest, queryCondition, tagoptionid);
        List<TagTypeVo> tagTypeVos = tagController.listTags();
        
        ModelAndView mv = new ModelAndView();
        mv.setViewName("blogs.html");
        mv.addObject("queryCondition", queryCondition);
        mv.addObject("tags", tagTypeVos);
        mv.addObject("systemParameter", this.systemParameterController.listSysParams());
        mv.addObject("es", this.es);
        if(!StringUtil.isEmpty(tagoptionid)) {
            mv.addObject("tagoptionid", tagoptionid);
            mv.addObject("tagoptionname", this.tagOptionDao.findById(tagoptionid).orElse(new TagOption()).getOptionname());
        }
        return mv;
    }
    
    
    // 将Blog对象转换为BlogVo对象
    private BlogVo convertBlog2BlogVo(Blog blog) {
        BlogVo blogVo = new BlogVo();
        blogVo.setId(blog.getId());
        blogVo.setTitle(blog.getTitle());

        blog.getTags().stream().forEach(tag -> {
            TagOptionVo tagOptionVo = new TagOptionVo();
            tagOptionVo.setOptionid(tag.getOptionid());
            tagOptionVo.setOptionname(tag.getOptionname());
            blogVo.getTags().add(tagOptionVo);
        });
        blogVo.setCreateBy(blog.getCreateBy());
        blogVo.setCreateDate(blog.getCreateDate().format(DateTimeFormatter.ofPattern(DateTimeUtil.DATETIMEFORMAT002)));
        blogVo.setModifyBy(blog.getModifyBy());
        blogVo.setUpdateDate(blog.getUpdateDate().format(DateTimeFormatter.ofPattern(DateTimeUtil.DATETIMEFORMAT002)));
        blogVo.setStar(blog.getStar());

        return blogVo;
    }
    

    //根据pageRequest对象查询数据库,并将查询出来的博客列表解析到queryCondition对象
    private QueryCondition<List<BlogVo>> queryNConvert(PageRequest pageRequest, QueryCondition<List<BlogVo>> queryCondition, String tagoptionid) {
        Page<Blog> blogs = this.blogDao.findAll(new Specification<Blog>() {
            private static final long serialVersionUID = -3977016522575809216L;

            @Override
            public Predicate toPredicate(Root<Blog> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<Predicate>();
                if(!StringUtil.isEmpty(tagoptionid)) {
                    predicates.add(criteriaBuilder.equal(root.join("tags", JoinType.LEFT).get("optionid").as(String.class), tagoptionid));
                }
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        }, pageRequest);

        queryCondition.getPage().setTotal(blogs.getTotalElements());
        queryCondition.getPage().setPageSize(queryCondition.getPage().getPageSize());
        queryCondition.getPage().setCurrentPage(queryCondition.getPage().getCurrentPage());
        queryCondition.setData(new ArrayList<BlogVo>());

        blogs.forEach(blog -> {
            BlogVo blogVo = convertBlog2BlogVo(blog);
            queryCondition.getData().add(blogVo);
        });
        return queryCondition;
    }
}
