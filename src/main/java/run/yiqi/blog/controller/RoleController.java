package run.yiqi.blog.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.extern.slf4j.Slf4j;
import run.yiqi.blog.dao.RoleDao;
import run.yiqi.blog.model.Role;
import run.yiqi.blog.vo.AdminMenuVo;
import run.yiqi.blog.vo.RoleVo;

@Slf4j
@Controller
public class RoleController {
	
	@Lazy @Autowired
    private RoleDao roleDao;
	
	/**
	 * /api/{api type}/{module name}/{submodule name}/{api name}
	 */
	@ResponseBody
	@RequestMapping(value="/api/admin/role/query/listRoles", method=RequestMethod.GET)  
	public List<RoleVo> listRoles() {
	    log.info("listRoles");
		List<RoleVo> result = new ArrayList<RoleVo>();
		Iterable<Role> roles = this.roleDao.findAll(new Sort(Direction.ASC, "id"));
		
		roles.forEach(r->{
            RoleVo rv = new RoleVo();
            rv.setId(r.getId());
            rv.setName(r.getRoleName());
            
            r.getMenus().forEach(son->{
                AdminMenuVo mvs = new AdminMenuVo();
                mvs.setId(son.getId());
                mvs.setTitle(son.getTitle());
                mvs.setFid(son.getFather().getId());
                mvs.setFtitle(son.getFather().getTitle());
                mvs.setUrl(son.getUrl());
                rv.getMenus().add(mvs);
            });
            result.add(rv);
		});
		return result;
	}
}