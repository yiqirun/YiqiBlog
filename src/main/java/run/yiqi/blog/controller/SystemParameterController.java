package run.yiqi.blog.controller;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Base64.Encoder;

import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import run.yiqi.blog.dao.SystemParameterDao;
import run.yiqi.blog.model.SystemParameter;
import run.yiqi.blog.vo.SystemParameterVo;

@Slf4j
@Controller
public class SystemParameterController {

    @Lazy @Autowired
    private SystemParameterDao systemParameterDao;
    
    @Lazy @Autowired
    private HttpServletResponse response;
    
    /**
     * 列举所有系统参数
     * @return
     */
    @ResponseBody
    @RequestMapping(value="/api/admin/sysparam/query/listsysparams", method=RequestMethod.POST)  
    public SystemParameterVo listSysParams() {
        log.info("listSysParams");
       
        SystemParameterVo systemParameterVo = new SystemParameterVo();
        Iterable<SystemParameter> systemParameters = this.systemParameterDao.findAll();

       
        systemParameters.forEach(systemParameter->{
            
            switch(systemParameter.getKey()) {
            case "run.yiqi.blog.name": 
                try {
                    systemParameterVo.setBlogname(new String(systemParameter.getValue(), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            case "run.yiqi.meta.description": 
                try {
                    systemParameterVo.setBlogdesc(new String(systemParameter.getValue(), "UTF-8"));
                } catch (UnsupportedEncodingException e5) {
                    e5.printStackTrace();
                }
                break;
            case "run.yiqi.meta.keywords": 
                try {
                    systemParameterVo.setBlogkeywords(new String(systemParameter.getValue(), "UTF-8"));
                } catch (UnsupportedEncodingException e4) {
                    e4.printStackTrace();
                }
                break;
            case "run.yiqi.icon": 
                try {
                    systemParameterVo.setIcon(new String(systemParameter.getValue(), "UTF-8"));
                } catch (UnsupportedEncodingException e3) {
                    e3.printStackTrace();
                }
                break;
            case "run.yiqi.rights": 
                try {
                    systemParameterVo.setRights(new String(systemParameter.getValue(), "UTF-8"));
                } catch (UnsupportedEncodingException e2) {
                    e2.printStackTrace();
                }
                break;
            case "run.yiqi.register": 
                try {
                    systemParameterVo.setRegister(new String(systemParameter.getValue(), "UTF-8"));
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
                break;
            case "run.yiqi.contact": 
                try {
                    systemParameterVo.setContact(new String(systemParameter.getValue(), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
            }

        });
        return systemParameterVo;
    }
    
    
    /**
     * 保存系统参数
     * @param SystemParameterVo
     * @return
     */
    @Transactional
    @SneakyThrows
    @ResponseBody
    @RequestMapping(value="/api/admin/sysparam/admin/saveSystemParameter", method=RequestMethod.POST)  
    public SystemParameterVo saveSystemParameter(@RequestBody SystemParameterVo systemParameterVo) {
        log.info("saveSystemParameter");
        Iterable<SystemParameter> systemParameters = this.systemParameterDao.findAll();
        
        systemParameters.forEach(systemParameter->{
            
            switch(systemParameter.getKey()) {
            case "run.yiqi.blog.name": 
                try {
                    systemParameter.setValue(systemParameterVo.getBlogname().getBytes("UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            case "run.yiqi.meta.description": 
                try {
                    systemParameter.setValue(systemParameterVo.getBlogdesc().getBytes("UTF-8"));
                } catch (UnsupportedEncodingException e5) {
                    e5.printStackTrace();
                }
                break;
            case "run.yiqi.meta.keywords": 
                try {
                    systemParameter.setValue(systemParameterVo.getBlogkeywords().getBytes("UTF-8"));
                } catch (UnsupportedEncodingException e4) {
                    e4.printStackTrace();
                }
                break;
            case "run.yiqi.icon": 
                //try {
                //    systemParameter.setValue(systemParameterVo.getIcon().getBytes("UTF-8"));
                //} catch (UnsupportedEncodingException e3) {
                //    e3.printStackTrace();
                //}
                break;
            case "run.yiqi.rights": 
                try {
                    systemParameter.setValue(systemParameterVo.getRights().getBytes("UTF-8"));
                } catch (UnsupportedEncodingException e2) {
                    e2.printStackTrace();
                }
                break;
            case "run.yiqi.register": 
                try {
                    systemParameter.setValue(systemParameterVo.getRegister().getBytes("UTF-8"));
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
                break;
            case "run.yiqi.contact": 
                try {
                    systemParameter.setValue(systemParameterVo.getContact().getBytes("UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
            }

        });
        this.systemParameterDao.saveAll(systemParameters);
        return systemParameterVo;
    }
    
    /**
     * 上传ico文件
     * @param icon
     */
    @SneakyThrows
    @ResponseBody
    @RequestMapping(value="/api/admin/sysparam/admin/uploadIcon")
    public void uploadIco( @RequestParam(value="file", required=true) MultipartFile file) {
        log.info("uploadIcon");
        
        Encoder encoder = Base64.getEncoder();
        String base64 = new String("data:image/x-icon;base64,"+encoder.encodeToString(file.getBytes()));
        
        SystemParameter systemParameter = this.systemParameterDao.findByKey("run.yiqi.icon");
        systemParameter.setValue(base64.getBytes());
        this.systemParameterDao.save(systemParameter);
        return;
    }
    
    /**
     * 发送图标
     */
    @SneakyThrows
    @ResponseBody
    @RequestMapping(value="/api/img/favicon.ico", method=RequestMethod.GET)  
    public String favicon() {
        log.info("icon");
        SystemParameter systemParameter = this.systemParameterDao.findByKey("run.yiqi.icon");
        String icon = new String(systemParameter.getValue(), "UTF-8");
        log.info(icon);
        return icon;
        
        /*response.setContentType("image/x-icon");
        OutputStream os = response.getOutputStream();
        os.write(systemParameter.getValue());
        os.flush();
        os.close();*/
    }

}
