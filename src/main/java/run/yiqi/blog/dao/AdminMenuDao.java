package run.yiqi.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import run.yiqi.blog.model.AdminMenu;


public interface AdminMenuDao extends JpaSpecificationExecutor<AdminMenu>, JpaRepository<AdminMenu, Long> {
}
