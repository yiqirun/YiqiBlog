package run.yiqi.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import run.yiqi.blog.model.Blog;


public interface BlogDao extends JpaSpecificationExecutor<Blog>, JpaRepository<Blog, String> {
}
