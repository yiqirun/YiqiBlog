package run.yiqi.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import run.yiqi.blog.model.Role;

public interface RoleDao extends JpaSpecificationExecutor<Role>, JpaRepository<Role, Long> {
}
