package run.yiqi.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import run.yiqi.blog.model.SystemParameter;

public interface SystemParameterDao extends JpaSpecificationExecutor<SystemParameter>, JpaRepository<SystemParameter, String> {
    public SystemParameter findByKey(String key);
}
