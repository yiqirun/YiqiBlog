package run.yiqi.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import run.yiqi.blog.model.TagOption;

public interface TagOptionDao extends JpaSpecificationExecutor<TagOption>, JpaRepository<TagOption, String> {
}
