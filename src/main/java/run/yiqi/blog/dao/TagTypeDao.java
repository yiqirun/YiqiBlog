package run.yiqi.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import run.yiqi.blog.model.TagType;

public interface TagTypeDao extends JpaSpecificationExecutor<TagType>, JpaRepository<TagType, String> {
}
