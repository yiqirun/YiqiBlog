package run.yiqi.blog.dao.es;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import run.yiqi.blog.model.BlogEs;


public interface BlogEsDao extends ElasticsearchRepository<BlogEs, String> {
}
