package run.yiqi.blog.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class AdminMenu implements Serializable {
    private static final long serialVersionUID = -4461374781354762002L;

    @Id
    private Long id = 0L;

    @Column(nullable = false, unique = true)
    private String title;

    @Column(nullable = true)
    private String url;

    /**
     * 主动方
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "father_id", nullable = true)
    private AdminMenu father;

    /**
     * 被动方
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "father")
    private List<AdminMenu> children = new ArrayList<AdminMenu>();

    /**
     * 被动方
     */
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "menus")
    private Set<Role> roles = new HashSet<Role>();


}
