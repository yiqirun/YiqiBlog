package run.yiqi.blog.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.hibernate.annotations.Type;
import org.springframework.data.elasticsearch.annotations.Document;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
//似乎放在最后不会报错, 放在前面会报错, 原因不明
@Document(indexName="yiqiblog", type="blog")
public class BlogEs implements Serializable {
    private static final long serialVersionUID = 6961037288345418312L;

    //ID,自动生成唯一字符串
    @Id
    private String id;

    //问题标题
    @Column(nullable = false)
    private String title;

    //问题描述
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Basic(fetch = FetchType.LAZY)
    @Column(nullable = false)
    private String content;

}
