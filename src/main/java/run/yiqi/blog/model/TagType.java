package run.yiqi.blog.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class TagType implements Serializable {
    private static final long serialVersionUID = -4461374781354762002L;

    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @GeneratedValue(generator = "uuid")
    private String typeid;

    @Column(nullable = true)
    private String typename;

    /**
     * 被动方
     */
    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "tagtype")
    private List<TagOption> options = new ArrayList<TagOption>();

    @Column(nullable = true)
    private int sortOrder =0 ;
}
