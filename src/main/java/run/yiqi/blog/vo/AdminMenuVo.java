package run.yiqi.blog.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AdminMenuVo implements Serializable{
    private static final long serialVersionUID = -5550216144834136641L;

    private Long id = 0L;
    private String title;
    private String url;
    private Long fid = 0L;
    private String ftitle;

    private List<AdminMenuVo> children = new ArrayList<AdminMenuVo>();

}
