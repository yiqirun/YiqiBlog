package run.yiqi.blog.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class BlogVo implements Serializable {
    private static final long serialVersionUID = -520335175185023815L;

    private String id;
    private String title;
    private String content;
    
    private List<TagOptionVo> tags = new ArrayList<TagOptionVo>();

    private Boolean star = false;

    private String createDate;
    private String updateDate;
    private String createBy;
    private String modifyBy;
}
