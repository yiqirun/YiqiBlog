package run.yiqi.blog.vo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class EndUserVo implements Serializable {
    private static final long serialVersionUID = -2339738162035271674L;

    private Long id = 0L;
    private String username;
    private String password;
    private String confirmpassword;
    private Set<RoleVo> roles = new HashSet<RoleVo>();

    private Long[] roleIds;

}
