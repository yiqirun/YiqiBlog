package run.yiqi.blog.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RoleVo implements Serializable {
    private static final long serialVersionUID = 6961037288345418312L;

    private Long id = 0L;
    private String name;
    private List<AdminMenuVo> menus = new ArrayList<AdminMenuVo>();
}
