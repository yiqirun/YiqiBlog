package run.yiqi.blog.vo;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SystemParameterVo implements Serializable{
    private static final long serialVersionUID = -5550216144834136641L;

    private String blogname;
    private String blogdesc;
    private String blogkeywords;
    private String icon;
    private String rights;
    private String register;
    private String contact;

}
