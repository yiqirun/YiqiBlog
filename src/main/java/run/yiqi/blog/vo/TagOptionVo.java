package run.yiqi.blog.vo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class TagOptionVo implements Serializable {
    private static final long serialVersionUID = -5550216144834136641L;

    private String optionid;
    private String optionname;
    private String typeid;
    
    //实体数量:可以是博客数
    private int entitycount;
    
    private int sortOrder;
}
