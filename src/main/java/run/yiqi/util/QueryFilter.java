package run.yiqi.util;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 过滤器
 * @author Administrator
 *
 */
@Setter
@Getter
@ToString
public class QueryFilter {
    private String field;
    private Object[] values;
    
}