package run.yiqi.util;

import org.springframework.data.domain.Sort.Direction;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 排序
 * @author Administrator
 *
 */
@Setter
@Getter
@ToString
public class QuerySort {
    private String field;
    //ASC DESC
    private Direction direction;
}