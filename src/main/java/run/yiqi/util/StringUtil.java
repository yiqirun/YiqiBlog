package run.yiqi.util;

public class StringUtil {
    
    /**
     * 判断目标字符串是否为空
     * @param targetString
     * @return
     */
    public static boolean isEmpty (String targetString) {
        if(null == targetString || "".equals(targetString)) {
            return true;
        }
        return false;
    }
}
